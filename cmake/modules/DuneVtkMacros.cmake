find_package(ZLIB)

set(HAVE_VTK_ZLIB ${ZLIB_FOUND})
if(HAVE_VTK_ZLIB)
  dune_register_package_flags(
    LIBRARIES "ZLIB::ZLIB"
    COMPILE_DEFINITIONS "ENABLE_VTK_ZLIB=1")
endif()

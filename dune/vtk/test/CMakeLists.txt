# link all tests against Dune::Vtk
link_libraries(Dune::Vtk)

dune_add_test(NAME dunevtk_test_map_datatypes
              SOURCES test-map-datatypes.cc)

dune_add_test(NAME dunevtk_test_function
              SOURCES test-function.cc)

dune_add_test(NAME dunevtk_test_mixedgrid
              SOURCES test-mixedgrid.cc
              CMAKE_GUARD "dune-functions_FOUND AND dune-uggrid_FOUND")

dune_add_test(NAME dunevtk_test_typededuction
              SOURCES test-typededuction.cc)

dune_add_test(NAME dunevtk_test_vtkwriter
              SOURCES test-vtkwriter.cc)
// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <iostream>
#include <limits>
#include <vector>

#include <dune/common/test/testsuite.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/grid/uggrid.hh>

#include <dune/vtk/vtkreader.hh>
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#include <dune/vtk/datacollectors/discontinuouslagrangedatacollector.hh>
#include <dune/vtk/datacollectors/discontinuousdatacollector.hh>
#include <dune/vtk/datacollectors/continuousdatacollector.hh>
#include <dune/vtk/gridcreators/continuousgridcreator.hh>
#include <dune/vtk/gridcreators/discontinuousgridcreator.hh>
#include <dune/vtk/gridcreators/lagrangegridcreator.hh>


int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

  const int dim = 2;
  using Grid = Dune::UGGrid<dim>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  auto gridPtr = factory.createGrid();
  auto& grid = *gridPtr;

  grid.globalRefine(1);

  using namespace Dune::Functions::BasisFactory;
  auto basis = makeBasis(grid.leafGridView(), lagrange<2>());

  auto f = [](auto x) { return x[0]*x[0] + x[1]*x[1]; };

  std::vector<double> coeff;
  coeff.resize(basis.dimension());

  interpolate(basis, coeff, f);

  auto gf = Dune::Functions::makeDiscreteGlobalBasisFunction<double>(basis, coeff);

  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::DiscontinuousLagrangeDataCollector;
  using Dune::Vtk::ContinuousDataCollector;
  using Dune::Vtk::DiscontinuousDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  using Dune::Vtk::VtkReader;
  using Dune::Vtk::ContinuousGridCreator;
  using Dune::Vtk::DiscontinuousGridCreator;
  using Dune::Vtk::LagrangeGridCreator;

  Dune::TestSuite test;

  {
    auto vtkWriter = UnstructuredGridWriter(ContinuousDataCollector(basis.gridView()),Dune::Vtk::FormatTypes::ASCII);
    vtkWriter.addPointData(gf, Dune::VTK::FieldInfo("x", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("vtk-test-continuous.vtu");

    auto gridCreator = ContinuousGridCreator<Grid>{};
    auto vtkReader = VtkReader(gridCreator);
    vtkReader.read("vtk-test-continuous.vtu");
    auto gridPtr2 = vtkReader.createGrid();
    test.check(gridPtr->size(0) == gridPtr2->size(0), "check ContinuousGridCreator size(0)");
    test.check(gridPtr->size(Dune::GeometryTypes::cube(2)) == gridPtr2->size(Dune::GeometryTypes::cube(2)), "check ContinuousGridCreator size(cube)");
    test.check(gridPtr->size(Dune::GeometryTypes::simplex(2)) == gridPtr2->size(Dune::GeometryTypes::simplex(2)), "check ContinuousGridCreator size(simplex)");
    test.check(gridPtr->size(dim) == gridPtr2->size(dim), "check ContinuousGridCreator size(dim)");
  }

  {
    auto vtkWriter = UnstructuredGridWriter(DiscontinuousDataCollector(basis.gridView()),Dune::Vtk::FormatTypes::ASCII);
    vtkWriter.addPointData(gf, Dune::VTK::FieldInfo("x", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("vtk-test-discontinuous.vtu");

    auto gridCreator = DiscontinuousGridCreator<Grid>{};
    auto vtkReader = VtkReader(gridCreator);
    vtkReader.read("vtk-test-discontinuous.vtu");
    auto gridPtr2 = vtkReader.createGrid();
    test.check(gridPtr->size(0) == gridPtr2->size(0), "check DiscontinuousGridCreator size(0)");
    test.check(gridPtr->size(Dune::GeometryTypes::cube(2)) == gridPtr2->size(Dune::GeometryTypes::cube(2)), "check DiscontinuousGridCreator size(cube)");
    test.check(gridPtr->size(Dune::GeometryTypes::simplex(2)) == gridPtr2->size(Dune::GeometryTypes::simplex(2)), "check DiscontinuousGridCreator size(simplex)");
    test.check(gridPtr->size(dim) == gridPtr2->size(dim), "check DiscontinuousGridCreator size(dim)");
  }

  {
    auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(basis.gridView(), 2),Dune::Vtk::FormatTypes::ASCII);
    vtkWriter.addPointData(gf, Dune::VTK::FieldInfo("x", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("vtk-test-lagrange.vtu");

    auto gridCreator = LagrangeGridCreator<Grid>{};
    auto vtkReader = VtkReader(gridCreator);
    vtkReader.read("vtk-test-lagrange.vtu");
    auto gridPtr2 = vtkReader.createGrid();
    test.check(gridPtr->size(0) == gridPtr2->size(0), "check LagrangeGridCreator size(0)");
    test.check(gridPtr->size(Dune::GeometryTypes::cube(2)) == gridPtr2->size(Dune::GeometryTypes::cube(2)), "check LagrangeGridCreator size(cube)");
    test.check(gridPtr->size(Dune::GeometryTypes::simplex(2)) == gridPtr2->size(Dune::GeometryTypes::simplex(2)), "check LagrangeGridCreator size(simplex)");
    test.check(gridPtr->size(dim) == gridPtr2->size(dim), "check LagrangeGridCreator size(dim)");
  }

  {
    auto vtkWriter = UnstructuredGridWriter(DiscontinuousLagrangeDataCollector(basis.gridView(), 2),Dune::Vtk::FormatTypes::ASCII);
    vtkWriter.addPointData(gf, Dune::VTK::FieldInfo("x", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("vtk-test-discontinuouslagrange.vtu");
  }

  return test.exit();
}
